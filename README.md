# npm notes

## npm - node package manager :

- NPM is own microsoft.
- NPM is a standard package manager for node since beginning of javascript project/nodejs.
- NPM is a package manager which supports javascript only and used for nodejs only.
- NPM is a kind of repository to host modules/packages.

### Difference between NPM and YARN

- YARN is created by facebook.
- YARN was born because npm was very slow for many operations there was no optimization and catching strategies and things.
- Both NPM and YARN are open sourced.
- YARN majorly works good on `dev server` and NPM on `production server`.
- Nowadays YARN and NPM are became almost similar.

### Installing `Node.Js` & **NPM**

For using NPM we need to have `nodejs` installed.

#### What is versioning in **`Node`** and **`NPM`**

This means using different versions of Node installed on the system through which we can run projects.

There might be some legacy code which might work for a perticular node version only.

#### How to do Node Versioning

- There is package called `NVM` `node version manager`, which helps us to do this on windows.
- In MacOs and Linux there is a package called `n`.

### What is a Node Module

- Node Modules are the already built projects, and we can use their functionalities using the npm.

What happens when npm installs a package :
What is inside a node package:

- ```
  npm view <package-name>
  ```
- ```bash
  $ npm view lodash

    lodash@4.17.21 | MIT | deps: none | versions: 114
    Lodash modular utilities.
    https://lodash.com/

    keywords: modules, stdlib, util

    dist
    .tarball: https://registry.npmjs.org/lodash/-/lodash-4.17.21.tgz
    .shasum: 679591c564c3bffaae8454cf0b3df370c3d6911c
    .integrity: sha512-v2kDEe57lecTulaDIuNTPy3Ry4gLGJ6Z1O3vE1krgXZNrsQ+LFTGHVxVjcXPs17LhbZVGedAJv8XZ1tvj5FvSg==
    .unpackedSize: 1.4 MB

    maintainers:

    - mathias <mathias@qiwi.be>
    - jdalton <john.david.dalton@gmail.com>
    - bnjmnt4n <benjamin@dev.ofcr.se>

    dist-tags:
    latest: 4.17.21

    published over a year ago by bnjmnt4n <benjamin@dev.ofcr.se>
  ```

  .tgz is a important file in the package npm repo that has everything about the package.

### `package.json` file

- `package.json` file is a record which stores the metadata about the project and the information about the all the packages which we have installed and on which our project is dependant.

- **_`npm init` :_** This is basically used for initialising the npm in the folder. It will ask some questions about the package and then automatically generates the `package.json` file.
- **_`npm init --y` :_** This will directly initailise a project without asking more questions.

- ```bash
  package.json ->
  {
  "dependencies": {
  "lodash": "^4.17.21"
  },
  "name": "npm_basics",
  "version": "1.0.0",
  "main": "index.js",
  "devDependencies": {},
  "scripts": {
  "test": "echo \"Error: no test specified\" && exit 1"
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "description": ""
  }
  ```

### Installing npm package

- **Local installation :**

```bash
npm install <package_name>
or
npm i <package_name>
```

Example

```bash
npm i lodash
```

- **Global installation :**
  **This `-g` is a shorthand of `-gloabal` flag shows that this package will be downloaded as a global package so that we can access it anywhere.**

```bash
npm i -g <package_name>
```

- Example:

```bash
npm i -g nodemon
```

#### How to remove packages:

- ```bash
    npm remove <package_name>
  ```

- ```bash
    npm uninstalled <package_name>
  ```

#### Local vs Global modules :

- Global modules should not be a dependency in a project.
- Global modules mostly created for using them through CLI.

### Semantic versioning in NPM :

- Semantic version means a version created using following some rules.
- Every package has a semantic version.
- Example : **_`15.6.8`_**

  ```
  There are 3 numbers sepated by . (dot)

  major update
  minor update
  patch update
  ```

  - package.json have the data about the dependencies which have stored the information about the versions of the packages installed.
  - If want to share list of dependencies then only share the package.json it will automatically download the dependencies.

  ```bash
      npm install
      or
      npm i
  ```

#### package-lock.json

- It stores all the information about the dependencies and the dependencies of the dependencies.
  - Ex. Our project is depends on the lodash, lodash depends on other packages those packages depends upon other packages.
- package-lock.json locks all the information and versions of all the packages.

#### Semantic versioning in package.json

- `(^) caret symbol`: It means any version should match the `major update `and any minor or patch update will work.
  - ```
      Ex. 4.x.x
      This means Version 4 with any minor,patch update.
    ```
- `(~) tilda symbol`: It means any version should match the `major and minor update number`and any patch update will work.
  - ```
      Ex. 4.7.x
      This means Version 4.7. with patch update.
    ```
- `without any symbol`: It means any version should match the `major and minor and patch update number`. Exact version.
  - ```
      Ex. 4.7.8
      This means exact 4.7.8.
    ```

### `dependencies` :

- `dependencies` object inside the `package.json` stores the list of packages on which our project depends.

  - Whenever we install any package that time it's added inside `dependencies` object.
  - **Ex.**

    ```bash
    npm i lodash

    package.json ->
            {
                "dependencies": {
                    "lodash": "^4.17.21"
                },
                "name": "npm_basics",
                "version": "1.0.0",
                "main": "index.js",
                "scripts": {
                    "test": "echo \"Error: no test specified\" && exit 1"
                },
                "keywords": [],
                "author": "",
                "license": "ISC",
                "description": ""
            }

    ```

- `devDependencies` object inside the `package.json` stores the developer or development dependencies which are helpful while developement only.

  - When we install any package with `--save-dev` flag in the end then it is saved as the devDependencies inside `devDependencies` Object.
  - **Ex.**

    ```bash
    npm i nodemon --save-dev

    package.json ->
        {
        "dependencies": {
            "lodash": "^4.17.21"
        },
        "name": "npm_basics",
        "version": "1.0.0",
        "main": "index.js",
        "devDependencies": {
            "nodemon": "^3.1.0"
        },
        "scripts": {
            "test": "echo \"Error: no test specified\" && exit 1"
        },
        "keywords": [],
        "author": "",
        "license": "ISC",
        "description": ""
        }
    ```

  - devDependencies are work inside the devlopement mode only. If we get into the production mode using

    - `NODE_ENV=production` devDependencies will not work there.

  - `peerDependencies` : Peer dependencies are the packages that are required to be installed in the user's environment. These packages are included in the peerDependencies section in the package.json file. These packages are not installed when you run npm install , but the user is expected to install them manually.

### `npm-scripts`

- Inside `package.json` there is nested object called `"scripts"`, inside that we can write some shorthand scripts which can be used for performing some tasks.
- **prepare (since npm@4.0.0)**

  ```bash
  Runs BEFORE the package is packed
  Runs BEFORE the package is published
  Runs on local npm install without any arguments
  Run AFTER prepublish, but BEFORE prepublishOnly
  NOTE: If a package being installed through git contains a prepare script, its dependencies and devDependencies will be installed, and the prepare script will be run, before the package is packaged and installed.

  prepublish (DEPRECATED)
  Same as prepare
  ```

  - Inside this scripts there are some predified keys and we can add some custom scripts inside that as well.
  - **_Example_**

    ```bash
    {
    "dependencies": {
        "lodash": "^4.17.21",
        "react-dom": "^18.2.0"
    },
    "name": "npm_basics",
    "version": "1.0.0",
    "main": "index.js",
    "devDependencies": {
        "nodemon": "^3.1.0"
    },
    "scripts": {
        "test": "echo \"Error: no test specified\" && exit 1",
        "start":"echo \"This is a start script\"",
        "customScript":"echo \" This is custom script\""
    },
    "keywords": [],
    "author": "",
    "license": "ISC",
    "description": ""
    }

    ```

  - The scripts which are already available can be run using npm only without using extra commands.

    - Example

      ```bash
      npm start
      ```

      This will work fine as start is already present in the list of the npm scripts.

      But if we will run

      ```bash
      npm customScript
      ```

      This will give a error that for running such a custom or not already available script we need to use run.

      ```bash
      npm run customScript
      ```

      Now it will work fine.

## NPX - Node Package Executor

- The `npx stands for Node Package Execute` and it comes with the npm, when you installed npm above 5.2. 0 version then automatically npx will installed. It is an npm package runner that can execute any package that you want from the npm registry without even installing that package.
- The biggest example of this is

  - `create-react-app` package.
  - When we want to create a react app that time mostly used command is `npx create-react-app` this simply means **_temporarily install_** the `create-react-app` package and **_run it immediatly_** and immediatly after performing that `create-react-app` operation delete that package from temp files as well.

- `Example :` There is a cowsay package which has a method cowsay which says anything which we give it so say.

  - As this is a node package but we dont want to install it.
  - We will use npx.

    ```bash
    $ npx cowsay hii nayan

        ---------------
        < hii nayan >
        ---------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
    ```

- `npx` is used while playing with the bash files as we can directly use this using the command lines. We can run `CLI` using `npx` indirectly.

### **_MISC Intro :_**

- **_CLI to check location where the modules are stored by default._**

  ```bash
  npm root -g
  ```

   ```bash
   C:\Users\Coditas\AppData\Roaming\npm\node_modules
   ```

- **_How to change location where the global packages are installed by default._**
    - There is something called prefix inside the config of npm which has the path where the global modules are stored. 
  ```bash
    $ npm config get prefix
    C:\Users\Admin\AppData\Roaming\npm

    /npm-codedamn/npm_basics (main)
    $ ls 
    node_modules/  package.json  package-lock.json

    /npm-codedamn/npm_basics (main)
    $ cd ..

    /npm-codedamn(main)
    $ mkdir globalNodeModules // creating new directory for global module

    nayan.sayaji_coditas@5575-LAP-0406 MINGW64 ~/Desktop/Assignments/npm-codedamn 
    (main)
    $ cd globalNodeModules/

    /npm-codedamn/globalNodeModules (main)
    $ pwd
    /c/Users/Admin/Desktop/Assignments/npm-codedamn/globalNodeModules

    /npm-codedamn/globalNodeModules (main)
    $ npm config set prefix /c/Users/Admin/Desktop/Assignments/npm-codedamn/globalNodeModules

    // new global path is set now
  ```


### Cache management npm :
- What is npm caching?
npm stores cache data in an opaque directory within the configured cache , named _cacache . This directory is a cacache -based content-addressable cache that stores all http request data as well as other package-related data.
- `removing npm-cache`:
    -  `npm cache clean --force`
    - Note that this is typically unnecessary, as npm's cache is self-healing and resistant to data corruption issues. verify: Verify the contents of the cache folder, garbage collecting any unneeded data, and verifying the integrity of the cache index and all cached data.